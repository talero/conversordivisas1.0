package conversor;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;

import calc.MoneyExchange;
import java.awt.Font;
import javax.swing.SwingConstants;

public class PanelExchange extends JPanel {
	
	private JTextField textMoneyValue;
	String moneyValue = "";
	String moneyToConvert = "";
	String moneyConverted= "";
	double valueIn = 0.0;
	double result = 0.0;
	
	private ResultListener listener;

	
	/**
	 * Create the panel.
	 */
	public PanelExchange() {
		setBounds(0,0,473,216);
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setLayout(null);
		
		JLabel labelMoneyValue = new JLabel("Ingrese el valor a convertir");
		labelMoneyValue.setFont(new Font("Dialog", Font.BOLD, 14));
		labelMoneyValue.setBounds(40, 29, 199, 27);
		add(labelMoneyValue);
		
		textMoneyValue = new RoundJTextField(15);
		//textMoneyValue = new JTextField();
		textMoneyValue.setHorizontalAlignment(SwingConstants.RIGHT);
		textMoneyValue.setFont(new Font("Dialog", Font.PLAIN, 14));
		textMoneyValue.setBounds(301, 31, 124, 21);
		add(textMoneyValue);
		textMoneyValue.setColumns(10);
			
		JLabel labelConvert = new JLabel("Convertir de :");
		labelConvert.setFont(new Font("Dialog", Font.BOLD, 14));
		labelConvert.setBounds(40, 78, 128, 17);
		add(labelConvert);
		
		JLabel labelMoneyTo = new JLabel("A :");
		labelMoneyTo.setFont(new Font("Dialog", Font.BOLD, 14));
		labelMoneyTo.setBounds(257, 78, 60, 17);
		add(labelMoneyTo);
		
		String[] options = {"Pesos", "Dolar EstadoUnidense","Euros", "Libras Esterlinas", "Yen Japones", "Won Surcoreanos"};
		
		// Creacion del JComboBox y añadir los items.
		JComboBox<String> comboBoxMoneyBase = new JComboBox<>(options);
		comboBoxMoneyBase.setFont(new Font("Dialog", Font.BOLD, 14));
		moneyValue = comboBoxMoneyBase.getSelectedItem().toString();
		comboBoxMoneyBase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moneyValue = comboBoxMoneyBase.getSelectedItem().toString();
			}
		});
		comboBoxMoneyBase.setBounds(40, 107, 181, 26);
		add(comboBoxMoneyBase);
		

		// Creacion del JComboBox y añadir los items.
		JComboBox<String> comboBoxMoneyToConvert = new JComboBox<>(options);
		comboBoxMoneyToConvert.setFont(new Font("Dialog", Font.BOLD, 14));
		moneyToConvert = comboBoxMoneyToConvert.getSelectedItem().toString();
		comboBoxMoneyToConvert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moneyToConvert = comboBoxMoneyToConvert.getSelectedItem().toString();
			}
		});
		comboBoxMoneyToConvert.setBounds(257, 107, 168, 26);
		add(comboBoxMoneyToConvert);

		JButton buttonConvert = new JButton("Convertir");
		buttonConvert.setFont(new Font("Dialog", Font.BOLD, 14));
		
		buttonConvert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					valueIn = Double.valueOf(textMoneyValue.getText());
					MoneyExchange convertMoney = new MoneyExchange();
					result = convertMoney.moneyExchange(moneyValue, moneyToConvert, valueIn);
					moneyConverted = String.valueOf(result);
					passResult(valueIn + " " + moneyValue  + " equivalen a : " +  moneyConverted +" " + moneyToConvert );
                } catch (NumberFormatException event) {
                    JOptionPane.showMessageDialog(null, "Ingresa un valor numérico", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
			}
		});
		buttonConvert.setBounds(165, 161, 105, 27);
		add(buttonConvert);
	}
	
	public void setResultListener(ResultListener listener) {
		this.listener = listener;
	}
	
	private void passResult(String newText) {
		if (listener != null) {
			listener.onResultPassed(newText);
		}
	}
}
