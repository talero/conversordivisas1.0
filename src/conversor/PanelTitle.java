package conversor;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

public class PanelTitle extends JPanel {

	JLabel labelTitle;

	/**
	 * Create the panel.
	 */
	public PanelTitle() {
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setBounds(0,-66,473,51);
		setVisible(true);	
		setLayout(null);
		
		labelTitle = new JLabel("Bienvenido");
		labelTitle.setForeground(new Color(14, 22, 214));
		labelTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitle.setFont(new Font("Dialog", Font.BOLD, 20));
		labelTitle.setBounds(12, 12, 449, 34);
		add(labelTitle);
	    
	}
	
	public void changeLabelTitle( String title) {
		labelTitle.setText(title);
	}
	
}
