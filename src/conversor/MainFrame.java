package conversor;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.UIManager;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	
	private PanelHome panelHome;
	private PanelExchange panelExchange;
	private PanelTemperature panelTemperature;
	private PanelLong panelLong;
	private PanelTitle panelTitle;
	private PanelResult panelResult;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
			      //      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			    //            if ("Nimbus".equals(info.getName())) {
			       //             UIManager.setLookAndFeel(info.getClassName());
			          //          break;
			          //      }
			        //    }
			     //   JFrame.setDefaultLookAndFeelDecorated(true);
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 727, 442);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 238, 238));
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Creacion de Paneles para las conversiones
		panelHome = new PanelHome();
		panelExchange = new PanelExchange();
		panelTemperature = new PanelTemperature();
		panelLong = new PanelLong();
		
		//Panel  Menu de Items
		JPanel panelMain = new JPanel();
		panelMain.setBackground(new Color(0, 0, 255));
		panelMain.setBounds(12, 12, 215, 417);
		contentPane.add(panelMain);
		panelMain.setLayout(null);
		
		JLabel labelLogo = new JLabel("");
		labelLogo.setIcon(new ImageIcon(MainFrame.class.getResource("/images/icons8-convert-96.png")));
		labelLogo.setBounds(54, 12, 90, 94);
		panelMain.add(labelLogo);
		
		//Item Home
		JPanel panelMainHome = new JPanel();
		panelMainHome.addMouseListener(new PanelButtonMouseAdapter(panelMainHome) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelHome);
				panelTitle.changeLabelTitle("Bienvenido");
			}
		});
		panelMainHome.setBackground(new Color(0, 0, 255));
		panelMainHome.setBounds(0, 124, 215, 50);
		panelMain.add(panelMainHome);
		panelMainHome.setLayout(null);
		
		JLabel labelIconHome = new JLabel("");
		labelIconHome.setIcon(new ImageIcon(MainFrame.class.getResource("/images/icons8-home-32.png")));
		labelIconHome.setBounds(25, 8, 32, 32);
		panelMainHome.add(labelIconHome);
		
		JLabel labelHome = new JLabel("Home");
		labelHome.setForeground(Color.WHITE);
		labelHome.setFont(new Font("Dialog", Font.BOLD, 18));
		labelHome.setBounds(75, 10, 54, 26);
		panelMainHome.add(labelHome);
		
		//Item Divisas
		JPanel panelMainExchange = new JPanel();
		panelMainExchange.addMouseListener(new PanelButtonMouseAdapter(panelMainExchange) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelExchange);
				panelTitle.changeLabelTitle("Conversor de Divisas");
			}
		});
		panelMainExchange.setBackground(new Color(0, 0, 255));
		panelMainExchange.setBounds(0, 174, 215, 50);
		panelMain.add(panelMainExchange);
		panelMainExchange.setLayout(null);
		
		JLabel labelForex = new JLabel("Divisas");
		labelForex.setFont(new Font("Dialog", Font.BOLD, 18));
		labelForex.setBounds(75, 10, 107, 26);
		labelForex.setForeground(new Color(255, 255, 255));
		panelMainExchange.add(labelForex);
		
		JLabel labelIconForex = new JLabel("");
		labelIconForex.setIcon(new ImageIcon(MainFrame.class.getResource("/images/exchange-32.png")));
		labelIconForex.setBounds(22, 8, 32, 32);
		panelMainExchange.add(labelIconForex);
		
		//Item temperatura
		JPanel panelMainTemperature = new JPanel();
		panelMainTemperature.addMouseListener(new PanelButtonMouseAdapter(panelMainTemperature) {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelTemperature);
				panelTitle.changeLabelTitle("Conversor de Temperatura");
			}
		});
		panelMainTemperature.setForeground(new Color(255, 255, 255));
		panelMainTemperature.setBackground(new Color(0, 0, 255));
		panelMainTemperature.setBounds(0, 224, 215, 50);
		panelMain.add(panelMainTemperature);
		panelMainTemperature.setLayout(null);
		
		JLabel labelTemperature = new JLabel("Temperatura");
		labelTemperature.setFont(new Font("Dialog", Font.BOLD, 18));
		labelTemperature.setBounds(75, 10, 128, 17);
		labelTemperature.setForeground(new Color(255, 255, 255));
		labelTemperature.setHorizontalAlignment(SwingConstants.CENTER);
		panelMainTemperature.add(labelTemperature);
		
		JLabel labelIconTemperature = new JLabel("");
		labelIconTemperature.setIcon(new ImageIcon(MainFrame.class.getResource("/images/temperature-32.png")));
		labelIconTemperature.setBounds(24, 8, 32, 32);
		panelMainTemperature.add(labelIconTemperature);
		
		//Item Longitud
		JPanel panelMainLong = new JPanel();
		panelMainLong.addMouseListener(new PanelButtonMouseAdapter(panelMainLong)  {
			@Override
			public void mouseClicked(MouseEvent e) {
				menuClicked(panelLong);
				panelTitle.changeLabelTitle("Conversor de Longitud");
			}
		});
		panelMainLong.setBackground(new Color(0, 0, 255));
		panelMainLong.setBounds(0, 274, 215, 50);
		panelMain.add(panelMainLong);
		panelMainLong.setLayout(null);
		
		JLabel labelLong = new JLabel("Longitud");
		labelLong.setFont(new Font("Dialog", Font.BOLD, 18));
		labelLong.setBounds(75, 10, 126, 17);
		labelLong.setForeground(new Color(255, 255, 255));
		panelMainLong.add(labelLong);
		
		JLabel labelIconLong = new JLabel("");
		labelIconLong.setIcon(new ImageIcon(MainFrame.class.getResource("/images/icons8-ruler-32.png")));
		labelIconLong.setBounds(25, 8, 32, 32);
		panelMainLong.add(labelIconLong);
		
		//Item Salir
		JPanel panelMainExit = new JPanel();
		panelMainExit.addMouseListener(new PanelButtonMouseAdapter(panelMainExit) {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if (JOptionPane.showConfirmDialog(null, "Desea salir de la aplicacion?")  == 0) {
					MainFrame.this.dispose();
				}
			}
		});
		panelMainExit.setLayout(null);
		panelMainExit.setBackground(new Color(0, 0, 255));
		panelMainExit.setBounds(0, 324, 215, 50);
		panelMain.add(panelMainExit);
		
		JLabel labelExit = new JLabel("Salir");
		labelExit.setForeground(Color.WHITE);
		labelExit.setFont(new Font("Dialog", Font.BOLD, 18));
		labelExit.setBounds(75, 10, 126, 17);
		panelMainExit.add(labelExit);
		
		JLabel labelIconExit = new JLabel("");
		labelIconExit.setIcon(new ImageIcon(MainFrame.class.getResource("/images/logout-32.png")));
		labelIconExit.setBounds(25, 8, 32, 32);
		panelMainExit.add(labelIconExit);
		
		//Panel del Titulo
		panelTitle = new PanelTitle();
		panelTitle.setBounds(239, 12, 473, 57);
		contentPane.add(panelTitle);
		
		//Panel de Resultados
		panelResult = new PanelResult();
		panelResult.setBounds(239, 309, 473, 120);
		contentPane.add(panelResult);
		panelExchange.setResultListener(panelResult);
		panelTemperature.setResultListener(panelResult);
		panelLong.setResultListener(panelResult);
		
		// Panel Principal que contiene los otros Panel
		JPanel panelMainContent = new JPanel();
		panelMainContent.setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		panelMainContent.setBounds(239, 81, 473, 216);
		contentPane.add(panelMainContent);
		panelMainContent.setLayout(null);
		
		panelMainContent.add(panelHome);
		panelMainContent.add(panelExchange);
		panelMainContent.add(panelTemperature);
		panelMainContent.add(panelLong);
		
		menuClicked(panelHome);
				
	}
	
	private class PanelButtonMouseAdapter extends MouseAdapter {
		
		JPanel panel;
		public PanelButtonMouseAdapter(JPanel panel) {
			this.panel = panel;
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			panel.setBackground(new Color( 98, 160, 234));
		}
		
		@Override
		public void mouseExited(MouseEvent e) {
			panel.setBackground(new Color(0, 0, 255));
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			panel.setBackground(new Color(192, 97, 230));
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			panel.setBackground(new Color(0, 0, 255));
		}
	}
	
	public void menuClicked(JPanel panel) {
		panelHome.setVisible(false);
		panelExchange.setVisible(false);
		panelTemperature.setVisible(false);
		panelLong.setVisible(false);
		panelResult.labelResult.setText(" ");
		
		panel.setVisible(true);
	}
 
}
