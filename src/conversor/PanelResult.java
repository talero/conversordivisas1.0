package conversor;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

public class PanelResult extends JPanel implements ResultListener{

	JLabel labelResult;
	/**
	 * Create the panel.
	 */
	public PanelResult() {
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setBounds(0,0,473,130);
		setLayout(null);
		setVisible(true);
		
		labelResult = new JLabel("");
		labelResult.setHorizontalAlignment(SwingConstants.CENTER);
		labelResult.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 14));
		labelResult.setBounds(22, 63, 422, 17);
		add(labelResult);
		
		JLabel labelResultTitle = new JLabel("Resultado de tu conversion");
		labelResultTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelResultTitle.setForeground(new Color(14, 22, 214));
		labelResultTitle.setFont(new Font("Dialog", Font.BOLD, 20));
		labelResultTitle.setBounds(12, 26, 449, 17);
		labelResultTitle.setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		add(labelResultTitle);
	}
	
	public void changeLabelResult( String result) {
		labelResult.setText(result);
	}

	@Override
	public void onResultPassed(String newText) {
		labelResult.setText(newText);
		
	}
	
	

}
