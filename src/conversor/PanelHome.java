package conversor;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class PanelHome extends JPanel {

	/**
	 * Create the panel.
	 */
	public PanelHome() {
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setLayout(null);
		setBounds(0,0,473,216);
		setVisible(true);	
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(98, 160, 234));
		panel.setBounds(12, 12, 146, 192);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PanelHome.class.getResource("/images/icons8-exchange-64 (1).png")));
		lblNewLabel.setBounds(35, 12, 60, 54);
		panel.add(lblNewLabel);
		
		JLabel labelInitForex = new JLabel("DIVISAS");
		labelInitForex.setHorizontalAlignment(SwingConstants.CENTER);
		labelInitForex.setFont(new Font("Dialog", Font.BOLD, 14));
		labelInitForex.setForeground(new Color(255, 255, 255));
		labelInitForex.setBounds(12, 75, 122, 26);
		panel.add(labelInitForex);
		
		JTextPane txtpnForex = new JTextPane();
		txtpnForex.setEditable(false);
		txtpnForex.setForeground(new Color(255, 255, 255));
		txtpnForex.setBackground(new Color(98, 160, 234));
		txtpnForex.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 12));
		txtpnForex.setText("Monedas oficiales distintas de la moneda legal en el propio país.");
		txtpnForex.setBounds(12, 100, 122, 78);
		panel.add(txtpnForex);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(98, 160, 234));
		panel_1.setLayout(null);
		panel_1.setBounds(167, 12, 143, 192);
		add(panel_1);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon(PanelHome.class.getResource("/images/icons8-temperature-64.png")));
		lblNewLabel_3.setBounds(28, 12, 60, 54);
		panel_1.add(lblNewLabel_3);
		
		JLabel lblNewLabel_1_1 = new JLabel("TEMPERATURA");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1_1.setFont(new Font("Dialog", Font.BOLD, 14));
		lblNewLabel_1_1.setBounds(12, 75, 119, 26);
		panel_1.add(lblNewLabel_1_1);
		
		JTextPane txtpnTemperature = new JTextPane();
		txtpnTemperature.setForeground(new Color(255, 255, 255));
		txtpnTemperature.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 12));
		txtpnTemperature.setBackground(new Color(98, 160, 234));
		txtpnTemperature.setText("Magnitud física que indica la energía interna de un cuerpo.");
		txtpnTemperature.setBounds(12, 100, 119, 79);
		panel_1.add(txtpnTemperature);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(98, 160, 234));
		panel_2.setLayout(null);
		panel_2.setBounds(318, 12, 143, 192);
		add(panel_2);
		
		JLabel lblNewLabel_1_2 = new JLabel("LONGITUD");
		lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_2.setFont(new Font("Dialog", Font.BOLD, 14));
		lblNewLabel_1_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_1_2.setBounds(12, 75, 131, 26);
		panel_2.add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_2_2 = new JLabel("");
		lblNewLabel_2_2.setIcon(new ImageIcon(PanelHome.class.getResource("/images/icons8-ruler-58.png")));
		lblNewLabel_2_2.setBounds(28, 12, 60, 54);
		panel_2.add(lblNewLabel_2_2);
		
		JTextPane txtpnLong = new JTextPane();
		txtpnLong.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 12));
		txtpnLong.setForeground(new Color(255, 255, 255));
		txtpnLong.setText("Magnitud física que expresa la distancia entre dos puntos");
		txtpnLong.setBackground(new Color(98, 160, 234));
		txtpnLong.setBounds(12, 100, 110, 68);
		panel_2.add(txtpnLong);

	}
}
