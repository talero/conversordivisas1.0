package conversor;
import javax.swing.JPanel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import calc.LongConvert;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JButton;

import javax.swing.SwingConstants;

public class PanelLong extends JPanel {
	
	private JTextField textLongIn;
	
	String longIn = "";
	String longOut = "";
	String longConverted= "";
	double result = 0.0;
	double valueIn = 0.0;

	private ResultListener listener;

	/**
	 * Create the panel.
	 */
	public PanelLong() {
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setBounds(0,0,473,216);
		setLayout(null);
		
		JLabel labelLongTitle = new JLabel("Ingrese el valor a convertir");
		labelLongTitle.setFont(new Font("Dialog", Font.BOLD, 14));
		labelLongTitle.setBounds(40, 29, 199, 27);
		labelLongTitle.setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		add(labelLongTitle);
		
		textLongIn = new RoundJTextField(15);
		textLongIn.setHorizontalAlignment(SwingConstants.RIGHT);
		textLongIn.setFont(new Font("Dialog", Font.PLAIN, 14));
		textLongIn.setBounds(301, 31, 124, 21);
		add(textLongIn);
		textLongIn.setColumns(10);
		
		JLabel labelLongConvert = new JLabel("Convertir de :");
		labelLongConvert.setFont(new Font("Dialog", Font.BOLD, 14));
		labelLongConvert.setBackground(Color.WHITE);
		labelLongConvert.setBounds(40, 78, 128, 17);
		add(labelLongConvert);
		
		JLabel labelLongTo = new JLabel("A :");
		labelLongTo.setFont(new Font("Dialog", Font.BOLD, 14));
		labelLongTo.setBackground(Color.WHITE);
		labelLongTo.setBounds(257, 78, 60, 17);
		add(labelLongTo);
		
		
		String[] options = {"Milimetros", "Decimetros","Centimetros","Metros", "Decametros","Hectometros","Kilometros"};
		
		JComboBox<String> comboBoxLongIn = new JComboBox<>(options);
		comboBoxLongIn.setFont(new Font("Dialog", Font.BOLD, 14));
		comboBoxLongIn.setBounds(40, 107, 181, 26);
		 longIn = comboBoxLongIn.getSelectedItem().toString();
		comboBoxLongIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				longIn = comboBoxLongIn.getSelectedItem().toString();
			}
		});
		add(comboBoxLongIn);
		
		JComboBox<String> comboBoxLongOut = new JComboBox<>(options);
		comboBoxLongOut.setFont(new Font("Dialog", Font.BOLD, 14));
		comboBoxLongOut.setBounds(257, 107, 168, 26);
		longOut = comboBoxLongIn.getSelectedItem().toString();
		comboBoxLongOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				longOut = comboBoxLongOut.getSelectedItem().toString();
			}
		});
		add(comboBoxLongOut);
		
		JButton buttonConvertLong = new JButton("Convertir");
		buttonConvertLong.setFont(new Font("Dialog", Font.BOLD, 14));
		buttonConvertLong.setBounds(165, 161, 105, 27);
		add(buttonConvertLong);
		buttonConvertLong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					valueIn = Double.valueOf(textLongIn.getText());
					LongConvert longitude= new LongConvert();
					result = longitude.longConvert(longIn, longOut, valueIn);
					longConverted = String.valueOf(result);
					passResult(valueIn + " " + longIn  + " equivalen a : " +  longConverted +" " + longOut);
				} catch (NumberFormatException event) {
                    JOptionPane.showMessageDialog(null, "Ingresa un valor numérico", "ERROR", JOptionPane.ERROR_MESSAGE);
                }	
			}
		});

	}
	
	public void setResultListener(ResultListener listener) {
		this.listener = listener;
	}
	
	private void passResult(String newText) {
		if (listener != null) {
			listener.onResultPassed(newText);
		}
	}

}
