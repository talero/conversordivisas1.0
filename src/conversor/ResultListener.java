package conversor;

public interface ResultListener {

	void onResultPassed(String newText);
}
