package conversor;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.UIManager;

import calc.TemperatureConvert;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelTemperature extends JPanel {
	private JTextField textTemeperatureIn;
	String tempertaureIn = "";
	String tempertaureOut = "";
	String temperatureConverted= "";
	double result = 0.0;
	double valueIn = 0.0;

	private ResultListener listener;

	/**
	 * Create the panel.
	 */
	public PanelTemperature() {
		setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		setBounds(0,0,473,216);
		setLayout(null);
		
		JLabel labelTemperatureIn = new JLabel("Ingrese el valor a convertir");
		labelTemperatureIn.setFont(new Font("Dialog", Font.BOLD, 14));
		labelTemperatureIn.setBounds(40, 29, 199, 27);
		labelTemperatureIn.setBackground(UIManager.getColor("TabbedPane.selectHighlight"));
		add(labelTemperatureIn);
		
		textTemeperatureIn = new RoundJTextField(15);
		textTemeperatureIn.setHorizontalAlignment(SwingConstants.RIGHT);
		textTemeperatureIn.setFont(new Font("Dialog", Font.PLAIN, 14));
		textTemeperatureIn.setBounds(301, 31, 124, 21);
		add(textTemeperatureIn);
		textTemeperatureIn.setColumns(10);
		
		JLabel labelConvert = new JLabel("Convertir de :");
		labelConvert.setFont(new Font("Dialog", Font.BOLD, 14));
		labelConvert.setBackground(Color.WHITE);
		labelConvert.setBounds(40, 78, 128, 17);
		add(labelConvert);
		
		JLabel labelTo = new JLabel("A :");
		labelTo.setFont(new Font("Dialog", Font.BOLD, 14));
		labelTo.setBackground(Color.WHITE);
		labelTo.setBounds(257, 78, 60, 17);
		add(labelTo);
		
		String[] options = {"Celsius", "Farenheith","Kelvin"};
		
		JComboBox<String> comboBoxTemperatureBase = new JComboBox<>(options);
		comboBoxTemperatureBase.setFont(new Font("Dialog", Font.BOLD, 14));
		comboBoxTemperatureBase.setBounds(40, 107, 181, 26);
		tempertaureIn = comboBoxTemperatureBase.getSelectedItem().toString();
		comboBoxTemperatureBase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tempertaureIn = comboBoxTemperatureBase.getSelectedItem().toString();
			}
		});
		add(comboBoxTemperatureBase);
		
		JComboBox<String> comboBoxTemperatureTo = new JComboBox<>(options);
		comboBoxTemperatureTo.setFont(new Font("Dialog", Font.BOLD, 14));
		comboBoxTemperatureTo.setBounds(257, 107, 168, 26);
		tempertaureOut = comboBoxTemperatureTo.getSelectedItem().toString();
		comboBoxTemperatureTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tempertaureOut = comboBoxTemperatureTo.getSelectedItem().toString();
			}
		});
		add(comboBoxTemperatureTo);
		
		JButton buttonConvertTemperature = new JButton("Convertir");
		buttonConvertTemperature.setFont(new Font("Dialog", Font.BOLD, 14));
		buttonConvertTemperature.setBounds(165, 161, 105, 27);
		add(buttonConvertTemperature);
		
		buttonConvertTemperature.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					valueIn = Double.valueOf(textTemeperatureIn.getText());
					TemperatureConvert temperature= new TemperatureConvert();
					result = temperature.temperatureConvert(tempertaureIn, tempertaureOut, valueIn);
					temperatureConverted = String.valueOf(result);
					passResult(valueIn + " grados " + tempertaureIn  + " equivalen a : " +  temperatureConverted  + " grados " +  tempertaureOut);
				}  catch (NumberFormatException event) {
                    JOptionPane.showMessageDialog(null, "Ingresa un valor numérico", "ERROR", JOptionPane.ERROR_MESSAGE);
                }	
			}
		});
	}
	
	public void setResultListener(ResultListener listener) {
		this.listener = listener;
	}
	
	private void passResult(String newText) {
		if (listener != null) {
			listener.onResultPassed(newText);
		}
	}

}
