package calc;

public class TemperatureConvert {

	public  double temperatureConvert (String temperatureIn, String temperatureOut, double valueIn) {
		
		double result;
		
		if (temperatureIn.equals( "Celsius")) {
			if(temperatureOut.equals("Farenheith")) {
				result = (valueIn * 1.8) + 32;
			} else if (temperatureOut.equals("Kelvin")){
				result = valueIn + 273.15;
			}else {
				result = valueIn;
			}
		} else if (temperatureIn.equals( "Farenheith")) {
			if(temperatureOut.equals("Celsius")) {
				result = (valueIn - 32) / 1.8;
			} else if (temperatureOut.equals("Kelvin")) {
				result = (valueIn + 459.67) * 0.55555556;
			} else {
				result = valueIn;
			}
		} else {
			if(temperatureOut.equals("Celsius")) {
				result = valueIn - 273.15;
			} else  if(temperatureOut.equals("Farenheith")){
				result = (valueIn *  0.55555556) - 459.67;
			} else {
				result = valueIn;
			}
		}
		return Math.round((result)*100.0)/100.0;
	}
	
}
