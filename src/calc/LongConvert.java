package calc;

/**
 * Clase para realizar la conversion de Longitudes
 */
public class LongConvert {

	double meter = 1.0;
	double milimeter = 0.001;
	double decimeter = 0.1;
	double centimeter = 0.01;
	double decameter = 10.0;
	double hectometer = 100.0;
	double kilometer = 1000.0;
	double longConverted = 0.0;

/**
 * Metodo pararealizar la conversion de Longitudes
 * @param longIn Escala de la Longitud ingresada
 * @param longOut Escala de Longitud a la que quiere convertir
 * @param valueIn Valor de la Longitud que quiere convertir
 * @return Valor de la Longitud convertida
 */
	public double longConvert(String longIn, String longOut, double valueIn) {

		switch (longIn) {

			case "Milimetros":
				longConverted = valueIn * milimeter;
				break;
			case "Centimetros":
				longConverted = valueIn * centimeter;
				break;
			case "Decimetros":
				longConverted = valueIn * decimeter;
				break;
			case "Metros":
				longConverted = valueIn * meter;
				break;
			case "Decametros":
				longConverted = valueIn * decameter;
				break;
			case "Hectometros":
				longConverted = valueIn * hectometer;
				break;
			case "Kilometros":
				longConverted = valueIn * kilometer;
				break;				
		}

		switch (longOut) {

			case "Milimetros":
				longConverted /= milimeter;
				break;
			case "Centimetros":
				longConverted /= centimeter;
				break;
			case "Decimetros":
				longConverted /= decimeter;
				break;
			case "Decametros":
				longConverted /= decameter;
				break;
			case "Hectometros":
				longConverted /= hectometer;
				break;
			case "Kilometros":
				longConverted /= kilometer;
				break;
		}

		return Math.round((longConverted)*100.0)/100.0;
	}
}
