package calc;

public class MoneyExchange {

	double dolar = 1.0;
	double euro = 0.91;
	double libra = 0.77;
	double yen = 142.20;
	double won = 1298.49;
	double peso = 4156.32;
	double moneyOut = 0.0;

	public double moneyExchange(String cashIn, String cashOut, double valueIn) {

		switch (cashOut) {
			case "Pesos":
				moneyOut = valueIn * peso;
				break;
			case "Dolar EstadoUnidense":
				moneyOut = valueIn * dolar;
				break;
			case "Euros":
				moneyOut = valueIn * euro;
				break;
			case "Libras Esterlinas":
				moneyOut = valueIn * libra;
				break;
			case "Yen Japones":
				moneyOut = valueIn * yen;
				break;
			case "Won Surcoreanos":
				moneyOut = valueIn * won;
				break;	
		}

		switch (cashIn) {
			case "Pesos":
				moneyOut /= peso;
				break;
			case "Dolar EstadoUnidense":
				moneyOut /= dolar;
				break;
			case "Euros":
				moneyOut /= euro;
				break;
			case "Libras Esterlinas":
				moneyOut /= libra;
				break;
			case "Yen Japones":
				moneyOut /= yen;
				break;
			case "Won Surcoreanos":
				moneyOut /= won;
				break;
		}

		return Math.round((moneyOut)*100.0)/100.0;
	}
}
